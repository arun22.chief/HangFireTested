﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Whois.NET;

namespace Hangfire.MvcApplication
{
    public static class TextBuffer
    {
        private static readonly StringBuilder Buffer = new StringBuilder();

        public static void WriteLine(string value)
        {
            lock (Buffer)
            {
                Buffer.AppendLine(String.Format("{0} {1}", DateTime.Now, value));
            }
        }


        public static void MyFunction(string value)
        {
            //   Thread.Sleep(4000);

            System.Net.WebClient wc = new System.Net.WebClient();

            byte[] raw = wc.DownloadData("https://www.google.co.in/search?q=school");
            string webData = System.Text.Encoding.UTF8.GetString(raw);

            MatchCollection matchList = Regex.Matches(webData, @"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}");
            var LinkCollection = matchList.Cast<Match>().Select(match => match.Value).ToList().Distinct().Skip(1);


            foreach (var Link in LinkCollection)
            {

                try
                {
                    //   var result =  WhoisClient.Query("techxora.com");
                    var FinalDomain = Link.Replace("http://www.", "").Replace("https://www.", "");
                    var result = WhoisClient.Query(FinalDomain);

                    string[] lines = result.Raw.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                    WriteLine(Link);
                    //foreach (var line in lines)
                    //{
                    //    var SplitLine = line.Split(':');
                    //    if (SplitLine.Contains("Domain Name"))
                    //    {
                    //        string DomainName = SplitLine[1];
                    //    };
                    //    if (SplitLine.Contains("Registry Domain ID"))
                    //    {
                    //        string RegistryDomainID = SplitLine[1];
                    //    }
                    //    if (SplitLine.Contains("Registrar URL"))
                    //    {
                    //        string RegistrarURL = SplitLine[1];
                    //    }
                    //    if (SplitLine.Contains("Registrant Name"))
                    //    {
                    //        string RegistrantName = SplitLine[1];
                    //    }
                    //    if (SplitLine.Contains("Registrant City"))
                    //    {
                    //        string RegistrantCity = SplitLine[1];
                    //    }
                    //    if (SplitLine.Contains("RegistrantStreet"))
                    //    {
                    //        string RegistrantStreet = SplitLine[1];
                    //    }
                    //    if (SplitLine.Contains("Registrant State/Province"))
                    //    {
                    //        string RegistrantStateProvince = SplitLine[1];
                    //    }
                    //    if (SplitLine.Contains("Registrant Postal Code"))
                    //    {
                    //        string RegistrantPostalCode = SplitLine[1];
                    //    }
                    //    if (SplitLine.Contains("Registrant Country"))
                    //    {
                    //        string RegistrantCountry = SplitLine[1];
                    //    }
                    //    if (SplitLine.Contains("Registrant Email"))
                    //    {
                    //        string RegistrantEmail = SplitLine[1];
                    //        WriteLine(RegistrantEmail);
                    //    }
                    //    if (SplitLine.Contains("Registrant Name"))
                    //    {
                    //        string RegistrantName = SplitLine[1];
                    //    }

                    //}


                    //DomainInfoTbl NewInfo = new DomainInfoTbl();
                    //NewInfo.DomainName = FinalDomain;
                    //NewInfo.DomainFullDescription = result.Raw.ToString();

                    //db.DomainInfoTbls.Add(NewInfo);
                    //db.SaveChanges();
                }
                catch (Exception e)
                {
                }


                // try
                // {
                //     byte[] rawHtmlWithAddress = wc.DownloadData(Link);
                //     string webDataWithAddress = System.Text.Encoding.UTF8.GetString(rawHtmlWithAddress);

                //     Regex emailRegex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                //RegexOptions.IgnoreCase);
                //     //find items that matches with our pattern
                //     MatchCollection emailMatches = emailRegex.Matches(webDataWithAddress);

                //     StringBuilder sb = new StringBuilder();

                //     foreach (Match emailMatch in emailMatches)
                //     {
                //         Console.Write(emailMatch.Value);
                //         sb.AppendLine(emailMatch.Value);
                //     }
                // }
                // catch (Exception e)
                // {
                // }
            }


            WriteLine("Job Completed");

        }


        public new static string ToString()
        {
            lock (Buffer)
            {
                return Buffer.ToString();
            }
        }
    }
}